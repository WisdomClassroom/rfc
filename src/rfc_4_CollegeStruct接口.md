# RFC-4 CollegeStruct 接口

## 接口服务

```
service CollegeStructService {
    rpc GetCollege          (GetCollegeRequest)          returns (GetCollegeResponse)          {}
    rpc ListCollege         (ListCollegeRequest)         returns (ListCollegeResponse)         {}
    rpc CreateCollege       (CreateCollegeRequest)       returns (CreateCollegeResponse)       {}
    rpc DeleteCollege       (DeleteCollegeRequest)       returns (DeleteCollegeResponse)       {}
    rpc SetCollegeManager   (SetCollegeManagerRequest)   returns (SetCollegeManagerResponse)   {}
    rpc UnSetCollegeManager (UnSetCollegeManagerRequest) returns (UnSetCollegeManagerResponse) {}
    rpc UpdateCollege       (UpdateCollegeRequest)       returns (UpdateCollegeResponse)       {}
}
```

## 接口请求响应子类型

```
message ListCollegeResponseUnit {
    string          UUID
    string          Name
    string          Description
    repeated string ManagerUUID
}
```

## 接口请求数据类型

```
message GetCollegeRequest {
    string CollegeUUID
    string AuthToken
}

message ListCollegeRequest {
    string AuthToken
}

message CreateCollegeRequest {
    string Name
    string Description
    string ManagerUUID
    string AuthToken
}

message DeleteCollegeRequest {
    string WillDeleteUUID
    string AuthToken
}

message SetCollegeManagerRequest {
    string WillSetCollegeUUID
    string WillSetManagerUUID
    string AuthToken
}

message UnSetCollegeManagerRequest {
    string WillUnSetCollegeUUID
    string WillUnSetManagerUUID
    string AuthToken
}

message UpdateCollegeRequest {
    string NewName 
	string NewDescription
    string WillUpdateUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetCollegeResponse {
    string          Name
    string          Description
    repeated string ManagerUUID
    ResponseStatus  Status
}

message ListCollegeResponse {
    repeated ListCollegeResponseUnit Colleges
    ResponseStatus                   Status
}

message CreateCollegeResponse {
    string         UUID
    ResponseStatus Status
}

message DeleteCollegeResponse {
    ResponseStatus Status
}

message SetCollegeManagerResponse {
    ResponseStatus Status
}

message UnSetCollegeManagerResponse {
    ResponseStatus Status
}

message UpdateCollegeResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
GET  /api/v1/collegeStruct/GetCollege/{CollegeUUID}/{AuthToken}
GET  /api/v1/collegeStruct/ListCollege/{AuthToken}
POST /api/v1/collegeStruct/CreateCollege
POST /api/v1/collegeStruct/DeleteCollege
POST /api/v1/collegeStruct/SetCollegeManager
POST /api/v1/collegeStruct/UnSetCollegeManager
POST /api/v1/collegeStruct/UpdateCollege
```
