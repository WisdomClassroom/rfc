# RFC-3 UserInfo 接口

## 接口服务

```
service UserInfoService {
    rpc Login          (LoginRequest)          returns (LoginResponse)          {}
    rpc SignUp         (SignUpRequest)         returns (SignUpResponse)         {}
    rpc UpdateInfo     (UpdateInfoRequest)     returns (UpdateInfoResponse)     {}
    rpc UpdatePassword (UpdatePasswordRequest) returns (UpdatePasswordResponse) {}
    rpc ResetPassword  (ResetPasswordRequest)  returns (ResetPasswordResponse)  {}
    rpc DeleteUser     (DeleteUserRequest)     returns (DeleteUserResponse)     {}
    rpc ChangeUserRole (ChangeUserRoleRequest) returns (ChangeUserRoleResponse) {}
    rpc FrozenAccount  (FrozenAccountRequest)  returns (FrozenAccountResponse)  {}
}
```

## 接口请求响应子类型

```
```

## 接口请求数据类型

```
message LoginRequest {
    int32  AccountID
    string PasswordMD5
}

message SignUpRequest {
    int32 AccountID
    string PasswordMD5
    string PasswordVerifyMd5
    string CollegeUUID
    string DisciplineUUID
    string ClassUUID
    UserType RoleType
    string AuthToken
}

message UpdateInfoRequest {
    string NewName 
	string NewGender
    string NewCollegeUUID
    string NewDisciplineUUID
    string NewClassUUID
    string AuthToken
}

messagedatePasswordRequest {
    string NewPasswordMD5
    string VerifyNewPasswordMD5
    string AuthToken
}

message ResetPasswordRequest {
    string AuthToken
}

message DeleteUserRequest {
    string WillDeleteUUID
    string AuthToken
}

message ChangeUserRoleRequest {
    string WillChangeUserUUID
    UserType NewRoleCode
    string AuthToken
}

message FrozenAccountRequest {
    string WillFrozenUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message LoginResponse {
    string         Token
    ResponseStatus Status
}

message SignUpResponse {
    string         Token
    ResponseStatus Status
}

message UpdateInfoResponse {
    string         NewToken
    ResponseStatus Status
}

message UpdatePasswordResponse {
    ResponseStatus Status
}

message ResetPasswordResponse {
    ResponseStatus Status
}

message DeleteUserResponse {
    ResponseStatus Status
}

message ChangeUserRoleResponse {
    ResponseStatus Status
}

message FrozenAccountResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
POST /api/v1/userInfo/Login
POST /api/v1/userInfo/SignUp
POST /api/v1/userInfo/UpdateInfo
POST /api/v1/userInfo/UpdatePassword
POST /api/v1/userInfo/ResetPassword
POST /api/v1/userInfo/DeleteUser
POST /api/v1/userInfo/ChangeUserRole
POST /api/v1/userInfo/FrozenAccount
```
