# Summary

- [RFC 1 服务程序设计](rfc_1_服务程序设计.md)
- [RFC 2 核心函数库设计](rfc_2_核心函数库设计.md)
- [RFC 3 UserInfo 接口](rfc_3_UserInfo接口.md)
- [RFC 4 CollegeStruct 接口](rfc_4_CollegeStruct接口.md)
- [RFC 5 DisciplineStruct 接口](rfc_5_DisciplineStruct接口.md)
- [RFC 6 ClassStruct 接口](rfc_6_ClassStruct接口.md)
- [RFC 7 Course 接口](rfc_7_Course接口.md)
- [RFC 8 Chapter 接口](rfc_8_Chapter接口.md)
- [RFC 9 全局接口类型](rfc_9_全局接口类型.md)
- [RFC 10 数据库模型](rfc_10_数据库模型.md)
- [RFC 11 QuestionBank 接口](rfc_11_QuestionBank接口.md)
- [RFC 12 Question 接口](rfc_12_Question接口.md)
