# RFC-6 ClassStruct 接口

## 接口服务

```
service ClassStructService {
    rpc GetClass            (GetClassRequest)            returns (GetClassResponse)            {}
    rpc ListClass           (ListClassRequest)           returns (ListClassResponse)           {}
    rpc ListCollegeClass    (ListCollegeClassRequest)    returns (ListCollegeClassResponse)    {}
    rpc ListDisciplineClass (ListDisciplineClassRequest) returns (ListDisciplineClassResponse) {}
    rpc CreateClass         (CreateClassRequest)         returns (CreateClassResponse)         {}
    rpc DeleteClass         (DeleteClassRequest)         returns (DeleteClassResponse)         {}
    rpc SetClassManager     (SetClassManagerRequest)     returns (SetClassManagerResponse)     {}
    rpc UnSetClassManager   (UnSetClassManagerRequest)   returns (UnSetClassManagerResponse)   {}
    rpc UpdateClass         (UpdateClassRequest)         returns (UpdateClassResponse)         {}
}
```

## 接口请求响应子类型

```
message ListClassResponseUnit {
    string          UUID       
    string          Name       
    string          Description
    string          CollegeUUID
    string          DisciplineUUID
    repeated string ManagerUUID
}
```

## 接口请求数据类型

```
message GetClassRequest {
    string ClassUUID
    string AuthToken
}

message ListClassRequest {
    string AuthToken
}

message ListCollegeClassRequest {
    string CollegeUUID
    string AuthToken 
}

message ListDisciplineClassRequest {
    string DisciplineUUID
    string AuthToken 
}

message CreateClassRequest {
    string Name       
    string Description
    string ManagerUUID
    string DisciplineUUID
    string AuthToken 
}

message DeleteClassRequest {
    string WillDeleteUUID
    string AuthToken 
}

message SetClassManagerRequest {
    string WillSetClassUUID
    string WillSetManagerUUID
    string AuthToken
}

message UnSetClassManagerRequest {
    string WillUnSetClassUUID
    string WillUnSetManagerUUID
    string AuthToken
}

message UpdateClassRequest {
    string NewName 
	string NewDescription
    string WillUpdateUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetClassResponse {
    string          Name
    string          Description
    string          CollegeUUID
    string          DisciplineUUID
    repeated string ManagerUUID
    ResponseStatus  Status
}

message ListClassResponse {
    repeated ListClassResponseUnit Class
    ResponseStatus                 Status
}

message ListCollegeClassResponse {
    repeated ListClassResponseUnit Class
    ResponseStatus                 Status
}

message ListDisciplineClassResponse {
    repeated ListClassResponseUnit Class
    ResponseStatus                 Status
}

message CreateClassResponse {
    string         UUID
    ResponseStatus Status
}

message DeleteClassResponse {
    ResponseStatus Status
}

message SetClassManagerResponse {
    ResponseStatus Status
}

message UnSetClassManagerResponse {
    ResponseStatus Status
}

message UpdateClassResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
GET  /api/v1/classStruct/GetClass/{ClassUUID}/{AuthToken}
GET  /api/v1/classStruct/ListClass/{AuthToken}
GET  /api/v1/classStruct/ListAllCollegeClass/{CollegeUUID}/{AuthToken}
GET  /api/v1/classStruct/ListAllDisciplineClass/{DisciplineUUID}/{AuthToken}
POST /api/v1/classStruct/CreateClass
POST /api/v1/classStruct/DeleteClass
POST /api/v1/classStruct/SetClassManager
POST /api/v1/classStruct/UnSetClassManager
POST /api/v1/classStruct/UpdateClass
```
