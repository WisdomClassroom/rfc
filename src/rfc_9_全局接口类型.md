# RFC-9 全局接口类型

```
enum ResponseStatusCode {
    ServerError    = 0
    Suc            = 1
    NotAuth        = 2
    FieldTypeError = 3
    OtherError     = 4
}

enum UserType {
    Undefine        = 0
    Root            = 1
    Maintainer      = 2
    Manager         = 3
    Leader          = 4
    Teacher         = 5
    Undergraduate   = 6
    GraduateStudent = 7
    DoctoralStudent = 8
}

message ResponseStatus {
    ResponseStatusCode Code
    string             Message
    string             Error
}

enum QuestionLevel {
    Basic = 0;
    Normal = 1;
    Difficult = 2;
    Challenging = 3;
}

message QuestionInfoUnit {
    string QuestionBase64 = 1;
    string AnalysisBase64 = 2;
    string AnswerBase64A = 3;
    string AnswerBase64B = 4;
    string AnswerBase64C = 5;
    string AnswerBase64D = 6;
    bool IsA = 7;
    bool IsB = 8;
    bool IsC = 9;
    bool IsD = 10;
    bool IsTrue = 11;
    QuestionType Type = 12;
    QuestionLevel Level = 13;
    string UUID = 14;
}

enum UserGender {
    Other = 0;
    Man = 1;
    WoMan = 2;
}

enum QuestionType {
    Single = 0;
    Multiple = 1;
    Judge = 2;
}
```
