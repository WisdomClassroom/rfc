# RFC-5 DisciplineStruct 接口

## 接口服务
```
service DisciplineStructService {
    rpc GetDiscipline          (GetDisciplineRequest)          returns (GetDisciplineResponse)          {}
    rpc ListDiscipline         (ListDisciplineRequest)         returns (ListDisciplineResponse)         {}
    rpc ListCollegeDiscipline  (ListCollegeDisciplineRequest)  returns (ListCollegeDisciplineResponse)  {}
    rpc CreateDiscipline       (CreateDisciplineRequest)       returns (CreateDisciplineResponse)       {}
    rpc DeleteDiscipline       (DeleteDisciplineRequest)       returns (DeleteDisciplineResponse)       {}
    rpc SetDisciplineManager   (SetDisciplineManagerRequest)   returns (SetDisciplineManagerResponse)   {}
    rpc UnSetDisciplineManager (UnSetDisciplineManagerRequest) returns (UnSetDisciplineManagerResponse) {}
    rpc UpdateDiscipline       (UpdateDisciplineRequest)       returns (UpdateDisciplineResponse)       {}
}
```


## 接口请求响应子类型

```                           
message ListDisciplineResponseUnit {
    string          UUID       
    string          Name       
    string          Description
    string          CollegeUUID
    repeated string ManagerUUID
}
```

## 接口请求数据类型

```
message GetDisciplineRequest {
    string DisciplineUUID
    string AuthToken 
}

message ListDisciplineRequest {
    string AuthToken 
}

message ListCollegeDisciplineRequest {
    string CollegeUUID
    string AuthToken 
}

message CreateDisciplineRequest {
    string Name       
    string Description
    string ManagerUUID
    string CollegeUUID
    string AuthToken 
}

message DeleteDisciplineRequest {
    string WillDeleteUUID
    string AuthToken 
}

message SetDisciplineManagerRequest {
    string WillSetDisciplineUUID
    string WillSetManagerUUID
    string AuthToken 
}

message UnSetDisciplineManagerRequest {
    string WillUnSetDisciplineUUID
    string WillUnSetManagerUUID
    string AuthToken 
}

message UpdateDisciplineRequest {
    string NewName 
	string NewDescription
    string WillUpdateUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetDisciplineResponse {
    string          Name       
    string          Description
    string          CollegeUUID
    repeated string ManagerUUID
    ResponseStatus  Status
}

message ListDisciplineResponse {
    repeated ListDisciplineResponseUnit Disciplines
    ResponseStatus                      Status
}
        
message ListCollegeDisciplineResponse {
    repeated ListDisciplineResponseUnit Disciplines
    ResponseStatus                      Status
}
        
message CreateDisciplineResponse {
    string         UUID
    ResponseStatus Status
}
        
message DeleteDisciplineResponse {
    ResponseStatus Status
}
        
message SetDisciplineManagerResponse {
    ResponseStatus Status
}
        
message UnSetDisciplineManagerResponse {
    ResponseStatus Status
}

message UpdateDisciplineResponse {
    ResponseStatus Status
}
````

## 接口 URL

```
GET  /api/v1/disciplineStruct/GetDiscipline/{DisciplineUUID}/{AuthToken}
GET  /api/v1/disciplineStruct/ListDiscipline/{AuthToken}
GET  /api/v1/disciplineStruct/ListCollegeDiscipline/{CollegeUUID}/{AuthToken}
POST /api/v1/disciplineStruct/CreateDiscipline
POST /api/v1/disciplineStruct/DeleteDiscipline
POST /api/v1/disciplineStruct/SetDisciplineManager
POST /api/v1/disciplineStruct/UnSetDisciplineManager
POST /api/v1/disciplineStruct/UpdateDiscipline
```
