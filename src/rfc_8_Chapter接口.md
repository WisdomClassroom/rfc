# RFC-8 Chapter 接口

## 接口服务

```
service ChapterService {
    rpc GetChapter          (GetChapterRequest)          returns (GetChapterResponse)          {}
    rpc GetContent          (GetContentRequest)          returns (GetContentResponse)          {}
    rpc ListChapter         (ListChapterRequest)         returns (ListChapterResponse)         {}
    rpc CreateChapter       (CreateChapterRequest)       returns (CreateChapterResponse)       {}
    rpc DeleteChapter       (DeleteChapterRequest)       returns (DeleteChapterResponse)       {}
    rpc SetChapterManager   (SetChapterManagerRequest)   returns (SetChapterManagerResponse)   {}
    rpc UnSetChapterManager (UnSetChapterManagerRequest) returns (UnSetChapterManagerResponse) {}
    rpc UpdateContent       (UpdateContentRequest)       returns (UpdateContentResponse)       {}
    rpc UpdateChapter       (UpdateChapterRequest)       returns (UpdateChapterResponse)       {}
}
```

## 接口请求响应子类型

```
message ListChapterResponseUnit {
    string          UUID
    string          Name
    string          Description
    repeated string ManagerUUID
    int32           Index
}
```

## 接口请求数据类型

```
message GetChapterRequest {
    string ChapterUUID
    string AuthToken
}

message GetContentRequest {
    string ChapterUUID
    string AuthToken
}

message ListChapterRequest {
    string CourseUUID
    string AuthToken
}

message CreateChapterRequest { 
    string Name
    string Description
    string ManagerUUID
    string CourseUUID 
    int32  Index
    string AuthToken
}

message DeleteChapterRequest { 
    string WillDeleteUUID 
    string AuthToken 
}

message SetChapterManagerRequest {
    string WillSetChapterUUID
    string WillSetManagerUUID
    string AuthToken
}

message UnSetChapterManagerRequest {
    string WillUnSetChapterUUID
    string WillUnSetManagerUUID
    string AuthToken
}

message UpdateContentRequest {
    string WillUpdateUUID
    string MarkdownBase64
    string AuthToken
}

message UpdateChapterRequest {
    string NewName 
	string NewDescription
    string WillUpdateUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetChapterResponse {
    string          Name
    string          Description
    repeated string ManagerUUID
    int32           Index
    ResponseStatus  Status
}

message GetContentResponse {
    string         MarkdownBase64
    int64          LastChange
    ResponseStatus Status
}

message ListChapterResponse {
    repeated ListChapterResponseUnit Chapter
    ResponseStatus                   Status
}

message CreateChapterResponse {
    string         UUID
    ResponseStatus Status
}

message DeleteChapterResponse {
    ResponseStatus Status
}

message SetChapterManagerResponse {
    ResponseStatus Status
}

message UnSetChapterManagerResponse {
    ResponseStatus Status
}

message UpdateContentResponse {
    ResponseStatus Status
}

message UpdateChapterResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
GET  /api/v1/chapter/GetChapter/{ChapterUUID}/{AuthToken}
GET  /api/v1/chapter/GetContent/{ChapterUUID}/{AuthToken}
GET  /api/v1/chapter/ListChapter/{CourseUUID}/{AuthToken}
POST /api/v1/chapter/CreateChapter
POST /api/v1/chapter/DeleteChapter
POST /api/v1/chapter/SetChapterManager
POST /api/v1/chapter/UnSetChapterManager
POST /api/v1/chapter/UpdateContent
POST /api/v1/chapter/UpdateChapter
```
