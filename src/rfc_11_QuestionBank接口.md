# RFC-11 QuestionBank 接口

## 接口服务

```
service QuestionBankService {
    rpc GetQuestionBank          (GetQuestionBankRequest)          returns (GetQuestionBankResponse)          {}
    rpc ListQuestionBank         (ListQuestionBankRequest)         returns (ListQuestionBankResponse)         {}
    rpc CreateQuestionBank       (CreateQuestionBankRequest)       returns (CreateQuestionBankResponse)       {}
    rpc UpdateQuestionBank       (UpdateQuestionBankRequest)       returns (UpdateQuestionBankResponse)       {}
    rpc DeleteQuestionBank       (DeleteQuestionBankRequest)       returns (DeleteQuestionBankResponse)       {}
    rpc SetQuestionBankManager   (SetQuestionBankManagerRequest)   returns (SetQuestionBankManagerResponse)   {}
    rpc UnSetQuestionBankManager (UnSetQuestionBankManagerRequest) returns (UnSetQuestionBankManagerResponse) {}
}
```

## 接口请求响应子类型

```
```

## 接口请求数据类型

```
message GetQuestionBankRequest {
    string QuestionBankUUID
    string AuthToken
}

message ListQuestionBankRequest {
    string CourseUUID
    string AuthToken
}

message CreateQuestionBankRequest {
    string Name       
    string Description
    string ManagerUUID
    string CourseUUID
    string AuthToken
}

message UpdateQuestionBankRequest {
    string Name       
    string Description
    string WillUpdateUUID
    string AuthToken
}

message DeleteQuestionBankRequest {
    string WillDeleteUUID
    string AuthToken
}

message SetQuestionBankManagerRequest {
    string WillSetQuestionBankUUID
    string WillSetManagerUUID
    string AuthToken
}

message UnSetQuestionBankManagerRequest {
    string WillUnSetQuestionBankUUID
    string WillUnSetManagerUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetQuestionBankResponse {
    string               Name
    string               Description
    repeated string      Managers
    ResponseStatus       Status
}

message ListQuestionBankResponse {
    string               UUID
    string               Name
    string               Description
    repeated string      Managers
    ResponseStatus       Status
}

message CreateQuestionBankResponse {
    string          UUID
    ResponseStatus  Status
}

message UpdateQuestionBankResponse {
    ResponseStatus Status
}

message DeleteQuestionBankResponse {
    ResponseStatus Status
}

message SetQuestionBankManagerResponse {
    ResponseStatus Status
}

message UnSetQuestionBankManagerResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
GET  /api/v1/questionBank/GetQuestionBank/{QuestionBankUUID}/{AuthToken}
GET  /api/v1/questionBank/ListQuestionBank/{CourseUUID}/{AuthToken}
POST /api/v1/questionBank/CreateQuestionBank
POST /api/v1/questionBank/UpdateQuestionBank
POST /api/v1/questionBank/DeleteQuestionBank
POST /api/v1/questionBank/SetQuestionBankManager
POST /api/v1/questionBank/UnSetQuestionBankManager
```
