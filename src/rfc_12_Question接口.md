# RFC-12 Question 接口

## 接口服务

```  
service QuestionService { 
    rpc GetQuestion    (GetQuestionRequest)    returns (GetQuestionResponse)    {}
    rpc ListQuestion   (ListQuestionRequest)   returns (ListQuestionResponse)   {}
    rpc ShowQuestions  (ShowQuestionsRequest)  returns (ShowQuestionsResponse)  {}
    rpc CreateQuestion (CreateQuestionRequest) returns (CreateQuestionResponse) {}
    rpc UpdateQuestion (UpdateQuestionRequest) returns (UpdateQuestionResponse) {}
    rpc DeleteQuestion (DeleteQuestionRequest) returns (DeleteQuestionResponse) {}
}
```

## 接口请求响应子类型

```
```

## 接口请求数据类型

```
message GetQuestionRequest {
    string QuestionUUID
    string AuthToken
}

message ListQuestionRequest {
    string QuestionBankUUID
    string AuthToken
}

message ShowQuestionsRequest {
    repeated string QuestionUUIDList
    string          AuthToken
}

message CreateQuestionRequest {
    QuestionInfoUnit  Question
    string            AuthToken
}

message UpdateQuestionRequest {
    QuestionInfoUnit  Question
    string            WillUpdateUUID
    string            AuthToken
}

message DeleteQuestionRequest {
    string WillDeleteUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetQuestionResponse {
    QuestionInfoUnit Question
    ResponseStatus   Status
}

message ListQuestionResponse {
    repeated QuestionInfoUnit Questions
    ResponseStatus            Status
}

message ShowQuestionsResponse {
    repeated QuestionInfoUnit Questions
    ResponseStatus            Status
}

message CreateQuestionResponse {
    string         UUID
    ResponseStatus Status
}

message UpdateQuestionResponse {
    ResponseStatus Status
}

message DeleteQuestionResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
GET  /api/v1/question/GetQuestion/{QuestionUUID}/{AuthToken}
GET  /api/v1/question/ListQuestion/{QuestionBankUUID}/{AuthToken}
POST /api/v1/question/ShowQuestions
POST /api/v1/question/CreateQuestion
POST /api/v1/question/UpdateQuestion
POST /api/v1/question/DeleteQuestion
```
