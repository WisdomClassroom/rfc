# RFC-7 Course 接口

## 接口服务

```
service CourseService {
    rpc GetCourse            (GetCourseRequest)            returns (GetCourseResponse)            {}
    rpc ListDisciplineCourse (ListDisciplineCourseRequest) returns (ListDisciplineCourseResponse) {}
    rpc ListCollegeCourse    (ListCollegeCourseRequest)    returns (ListCollegeCourseResponse)    {}
    rpc CreateCourse         (CreateCourseRequest)         returns (CreateCourseResponse)         {}
    rpc DeleteCourse         (DeleteCourseRequest)         returns (DeleteCourseResponse)         {}
    rpc SetCourseManager     (SetCourseManagerRequest)     returns (SetCourseManagerResponse)     {}
    rpc UnSetCourseManager   (UnSetCourseManagerRequest)   returns (UnSetCourseManagerResponse)   {}
    rpc UpdateCourse         (UpdateCourseRequest)         returns (UpdateCourseResponse)         {}
}
```

## 接口请求响应子类型

```
message ListCourseResponseUnit {
    string          UUID
    string          Name
    string          Description
    string          CollegeUUID
    string          DisciplineUUID
    repeated string ManagerUUID
}
```

## 接口请求数据类型

```
message GetCourseRequest {
    string CourseUUID
    string AuthToken
}

message ListDisciplineCourseRequest {
    string DisciplineUUID
    string AuthToken
}

message ListCollegeCourseRequest {
    string CollegeUUID
    string AuthToken
}

message CreateCourseRequest {
    string Name
    string Description
    string ManagerUUID
    string DisciplineUUID
    string AuthToken
}

message DeleteCourseRequest {
    string WillDeleteUUID
    string AuthToken
}

message SetCourseManagerRequest {
    string WillSetCourseUUID
    string WillSetManagerUUID
    string AuthToken
}

message UnSetCourseManagerRequest {
    string WillUnSetCourseUUID
    string WillUnSetManagerUUID
    string AuthToken
}

message UpdateCourseRequest {
    string NewName 
	string NewDescription
    string WillUpdateUUID
    string AuthToken
}
```

## 接口响应数据类型

```
message GetCourseResponse {
    string          Name
    string          Description
    repeated string ManagerUUID
    string          CollegeUUID
    string          DisciplineUUID
    ResponseStatus  Status
}

message ListDisciplineCourseResponse {
    repeated ListCourseResponseUnit Course
    ResponseStatus                  Status
}

message ListCollegeCourseResponse {
    repeated ListCourseResponseUnit Course
    ResponseStatus                  Status
}

message CreateCourseResponse {
    string         UUID
    ResponseStatus Status
}

message DeleteCourseResponse {
    ResponseStatus Status
}

message SetCourseManagerResponse {
    ResponseStatus Status
}

message UnSetCourseManagerResponse {
    ResponseStatus Status
}

message UpdateCourseResponse {
    ResponseStatus Status
}
```

## 接口 URL

```
GET  /api/v1/course/GetCourse/{CourseUUID}/{AuthToken}
GET  /api/v1/course/ListDisciplineCourse/{DisciplineUUID}/{AuthToken}
GET  /api/v1/course/ListCollegeCourse/{CollegeUUID}/{AuthToken}
POST /api/v1/course/CreateCourse
POST /api/v1/course/DeleteCourse
POST /api/v1/course/SetCourseManager
POST /api/v1/course/UnSetCourseManager
POST /api/v1/course/UpdateCourse
```
