# RFC-10 数据库模型

## 隶属关系

### 一对多
```
.
└── 课程
    ├── 班级
    └── 章节
        ├── 题目
        └── 知识点
```

### 多对多

```
班级 <-> 用户
题目 <-> 知识点
```

```
// 单引号包裹的为分类节点

'学院'
│
├───信息与计算机学院
│   │
│   ├───计算机科学与技术
│   │   │
│   │   ├───'班级'
│   │   │   │
│   │   │   ├───17级1班
│   │   │   │   │
│   │   │   │   ├───学生A
│   │   │   │   │
│   │   │   │   ├───学生B
│   │   │   │
│   │   │   ├───18级1班
│   │   │
│   │   ├───'课程'
│   │   │   │
│   │   │   ├───C语言
│   │   │   │   │
│   │   │   │   ├───'章节'
│   │   │   │   │   │
│   │   │   │   │   ├───运算符
│   │   │   │   │   │
│   │   │   │   │   ├───函数
│   │   │   │   │
│   │   │   │   ├───'题库'
│   │   │   │       │
│   │   │   │       ├───主题库
│   │   │   │           │
│   │   │   │           ├───题目A
│   │   │   │           │
│   │   │   │           ├───题目B
│   │   │   │
│   │   │   ├───Java语言
│   │   │   │
│   │   │   ├───数据库
│   │   │   │
```

## 通用模型

```go
type Model struct {
	ID           int64  `gorm:"not null; auto_increment"`
	UUID         string `gorm:"not null; type:char(36); unique_index"`
	Deleted      bool   `gorm:"not null; type:bool; default:false"`
	CreationTime int64  `gorm:"not null; type:bigint; default:-1"`
	UpdateTime   int64  `gorm:"not null; type:bigint; default:-1"`
	DeleteTime   int64  `gorm:"not null; type:bigint; default:-1"`
}
```

## 用户模型

```go
type UserModel struct {
	Model
	Type         int    `gorm:"not null; type:int; default:0"`
	AccountID    int    `gorm:"not null; type:int; unique_index:UserModel_accountID"`
	Name         string `gorm:"not null; type:varchar(32)"`
	Gender       int    `gorm:"not null; type:int; default:0"`
	PasswordSSHA string `gorm:"not null; type:varchar(128)"`
	PasswordSalt string `gorm:"not null; type:varchar(128)"`
}

type UserCollegeBindModel struct {
	Model
	UserUUID    string `gorm:"not null; type:char(36); index:userCollegeBindModel_userUUID"`
	CollegeUUID string `gorm:"not null; type:char(36); index:userCollegeBindModel_collegeUUID"`
}

type UserDisciplineBindModel struct {
	Model
	UserUUID       string `gorm:"not null; type:char(36); index:userDisciplineBindModel_userUUID"`
	DisciplineUUID string `gorm:"not null; type:char(36); index:userDisciplineBindModel_disciplineUUID"`
}

type UserClassBindModel struct {
	Model
	UserUUID  string `gorm:"not null; type:char(32); index:userClassBindModel_userUUID"`
	ClassUUID string `gorm:"not null; type:char(32); index:userClassBindModel_classUUID"`
}
```

## 学院结构模型

```go
type CollegeModel struct {
	Model
	Name        string `gorm:"not null; type:varchar(32)"`
	Description string `gorm:"not null; type:varchar(99)"`
}

type CollegeManagerBindModel struct {
	Model
	CollegeUUID string `gorm:"not null; type:char(36); index:collegeManagerBind_collegeUUID"`
	ManagerUUID string `gorm:"not null; type:char(36); index:collegeManagerBind_managerUUID"`
}

type CollegeDisciplineBindModel struct {
	Model
	CollegeUUID    string `gorm:"not null; type:char(36); index:collegeDisciplineBind_collegeUUID"`
	DisciplineUUID string `gorm:"not null; type:char(36); index:collegeDisciplineBind_disciplineUUID"`
}
```

## 专业结构模型

```go
type DisciplineModel struct {
	Model
	Name        string `gorm:"not null; type:varchar(32)"`
	Description string `gorm:"not null; type:varchar(99)"`
}

type DisciplineManagerBindModel struct {
	Model
	DisciplineUUID string `gorm:"not null; type:char(36); index:disciplineManagerBind_disciplineUUID"`
	ManagerUUID    string `gorm:"not null; type:char(36); index:disciplineManagerBind_managerUUID"`
}

type DisciplineClassBindModel struct {
	Model
	DisciplineUUID string `gorm:"not null; type:char(36); index:disciplineClassBind_disciplineUUID"`
	ClassUUID      string `gorm:"not null; type:char(36); index:disciplineClassBind_classUUID"`
}
```

## 班级结构模型

```go
type ClassModel struct {
	Model
	Name        string `gorm:"not null; type:varchar(32)"`
	Description string `gorm:"not null; type:varchar(99)"`
}

type ClassManagerBindModel struct {
	Model
	ClassUUID   string `gorm:"not null; type:char(36); index:classManagerBind_classUUID"`
	ManagerUUID string `gorm:"not null; type:char(36); index:classManagerBind_managerUUID"`
}
```

## 课程模型

```go
type CourseModel struct {
	Model
	Name        string `gorm:"not null; type:varchar(32)"`
	Description string `gorm:"not null; type:varchar(99)"`
}

type CourseManagerBindModel struct {
	Model
	CourseUUID  string `gorm:"not null; type:char(36); index:courseManagerBind_courseUUID"`
	ManagerUUID string `gorm:"not null; type:char(36); index:courseManagerBind_managerUUID"`
}

type CourseDisciplineBindModel struct {
	Model
	CourseUUID     string `gorm:"not null; type:char(36); index:courseDisciplineBind_courseUUID"`
	DisciplineUUID string `gorm:"not null; type:char(36); index:courseDisciplineBind_disciplineUUID"`
}
```

## 章节模型

```go
type ChapterModel struct {
	Model
	Name        string `gorm:"not null; type:varchar(32)"`
	Description string `gorm:"not null; type:varchar(99)"`
	ContentUUID string `gorm:"not null; type:char(36)"`
    Index       int    `gorm:"not null; type:int; default:1"`
}

type ContentModel struct {
	Model
	MarkdownBase64 string `gorm:"not null; type:text; default:''"`
}

type ChapterManagerBindModel struct {
	Model
	ChapterUUID string `gorm:"not null; type:char(36); index:chapterManagerBind_chapterUUID"`
	ManagerUUID string `gorm:"not null; type:char(36); index:chapterManagerBind_managerUUID"`
}
```

## 题库模型

```go
type BankModel struct {
	Model
	Name        string `gorm:"not null; type:varchar(32)"`
	Description string `gorm:"not null; type:varchar(99)"`
}

type BankCourseBindModel struct {
	Model
	BankUUID   string `gorm:"not null; type:char(36); index:bankCourseBind_bankUUID"`
	CourseUUID string `gorm:"not null; type:char(36); index:bankCourseBind_courseUUID"`
}

type BankManagerBindModel struct {
	Model
	BankUUID    string `gorm:"not null; type:char(36); index:bankManagerBind_bankUUID"`
	ManagerUUID string `gorm:"not null; type:char(36); index:bankManagerBind_managerUUID"`
}
```

## 题目模型

```go
type QuestionModel struct {
	Model
	QuestionBase64 string `gorm:"not null; type:varchar(512)"`
	AnalysisBase64 string `gorm:"not null; type:varchar(2048)"`
	AnswerBase64A  string `gorm:"not null; type:varchar(512)"`
	AnswerBase64B  string `gorm:"not null; type:varchar(512)"`
	AnswerBase64C  string `gorm:"not null; type:varchar(512)"`
	AnswerBase64D  string `gorm:"not null; type:varchar(512)"`
	IsA            bool   `gorm:"not null; type:bool"`
	IsB            bool   `gorm:"not null; type:bool"`
	IsC            bool   `gorm:"not null; type:bool"`
	IsD            bool   `gorm:"not null; type:bool"`
	IsTrue         bool   `gorm:"not null; type:bool"`
	Type           int    `gorm:"not null; type:int; default:0"`
	Level          int    `gorm:"not null; type:int; default:0"`
}

type QuestionBankBindModel struct {
	Model
	BankUUID     string `gorm:"not null; type:char(36); index:QuestionBankBind_bankUUID"`
	QuestionUUID string `gorm:"not null; type:char(36); index:QuestionBankBind_questionUUID"`
}
```
